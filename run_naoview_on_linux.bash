#!/usr/bin/env bash
docker run -ti --rm -e DISPLAY=$DISPLAY -v /tmp/.X11-unix:/tmp/.X11-unix --device=/dev/dri:/dev/dri -e QT_X11_NO_MITSHM=1 pepper_utils bash -c "/scripts/naoview.py --ip localhost"
