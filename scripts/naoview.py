#!/usr/bin/env python
# -*- coding: utf-8 -*-
# Copyright (C) Dyno Robotics AB - All Rights Reserved
# Unauthorized copying of this file, via any medium is strictly prohibited
# Proprietary and confidential
# Written by Erik Örjehag <erik@dynorobotics.se>, Mars 2019

import cv2
import numpy as np
import naoqi
from vision_definitions import *
import sys
import signal
import argparse
from random import randint


class NaoView:

    def __init__(self, ip, camera, resolution):
        #import matplotlib.image as mpimg
        #img = mpimg.imread('/scripts/dyno.png')
        #cv2.imshow("test window", img)
        #cv2.waitKey(1000)
        try:
            self.video_proxy = naoqi.ALProxy("ALVideoDevice", ip, 9559)
            self.camera_sub = self.video_proxy.subscribeCamera(
                "image_subs" + str(randint(1,1e9)), camera, resolution, kBGRColorSpace, 5)
            print("Created proxy!")
        except Exception as e:
            print("Error when creating video proxy: ")
            print(str(e))
            exit(1)
        self.win_name = "Stream"
        cv2.namedWindow(self.win_name,
                        cv2.WND_PROP_FULLSCREEN)
        cv2.setWindowProperty(self.win_name,
                              cv2.WND_PROP_FULLSCREEN,
                              cv2.WINDOW_FULLSCREEN)

    def close(self):
        self.video_proxy.unsubscribe(self.camera_sub)
        cv2.destroyAllWindows()

    def get_image_from_camera(self):
        """
        Return the image from camera or None on error.
        """
        image = self.video_proxy.getImageRemote(self.camera_sub)
        if image is not None:
            image = (
                np.reshape(np.frombuffer(image[6],
                                         dtype='%iuint8' % image[2]),
                           (image[1], image[0], image[2])
                           )
            )
        return image

    def display_image(self):
        img = self.get_image_from_camera()
        if img is not None:
            cv2.imshow(self.win_name, img)
            cv2.waitKey(1)
        else:
            print("img was None")


def parse_arguments():
    res = ['40x30', '80x60', '160x120', '320x240', '640x480', '1280x960']
    naores = [kQQQQVGA, kQQQVGA, kQQVGA, kQVGA, kVGA, k4VGA]
    cam = ['top', 'bottom']
    naocam = [kTopCamera, kBottomCamera]
    parser = argparse.ArgumentParser(description='Stream video from nao.')
    parser.add_argument('--ip', required=True)
    parser.add_argument('--camera', default=cam[0], choices=cam)
    parser.add_argument('--resolution', default=res[4], choices=res)
    args = parser.parse_args()
    return (
        args.ip,
        naocam[cam.index(args.camera)],
        naores[res.index(args.resolution)]
    )


interrupt = False


def signal_handler(sig, frame):
    global interrupt
    print("You pressed Ctrl+C!")
    interrupt = True


if __name__ == "__main__":

    signal.signal(signal.SIGINT, signal_handler)

    view = NaoView(*parse_arguments())

    while not interrupt:
        view.display_image()

    view.close()

    sys.exit(0)
