FROM ubuntu:xenial

# Terminal utils, etc...
RUN apt-get update && apt-get install -y \
  curl \
  wget \
  vim \
  less \
  python-pip \
  ranger \
  tmux \
  libyaml-dev \
  libxaw7-dev \
  clang-format \
  net-tools \
  iputils-ping \
  htop \
  libgl1-mesa-glx \
  libgl1-mesa-dri \
  mesa-utils \
  unzip \
  x11-xkb-utils \
  gedit \
  && rm -rf /var/likb/apt/lists/*

# Upgrade Mesa
RUN apt-get update && apt-get install -y \
    software-properties-common \
    && add-apt-repository -y ppa:ubuntu-x-swat/updates \
    && apt-get update && apt-get dist-upgrade -y \
    && rm -rf /var/likb/apt/lists/*

# Install naoqi
ADD pynaoqi-python2.7-2.1.3.3-linux64.tar.gz /
ENV PYTHONPATH $PYTHONPATH:/pynaoqi-python2.7-2.1.3.3-linux64
ENV LD_LIBRARY_PATH $LD_LIBRARY_PATH:/pynaoqi-python2.7-2.1.3.3-linux64

# Install OpenCv
RUN pip install --upgrade pip
RUN pip install opencv-python matplotlib

COPY scripts /scripts
